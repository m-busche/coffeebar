#!/usr/bin/env python3
__author__ = "Markus Busche"
__license__ = "MIT"
__version__ = "1.0.0"
__email__ = "markus@butenostfreesen.de"


import sys
from evdev import InputDevice, ecodes, list_devices, categorize

def getdevice():
    try:
        devices = map(InputDevice, list_devices())
        for device in devices:
            print(device.fn + '\t\t' + device.name)

    except:
        print('Keine Leseberechtigung für Geräte')
        sys.exit(1)


if __name__ == "__main__":
    print('List of input devices:\n')
    getdevice()
