#!/usr/bin/env python3
"""Script to read a Barcode scanner"""
__author__ = "Markus Busche"
__license__ = "MIT"
__version__ = "1.0.0"
__email__ = "markus@butenostfreesen.de"

import sys
import time
import logging
import requests
import os.path
from configparser import ConfigParser
import xlsxwriter
import xlrd
from pyfiglet import Figlet
from evdev import InputDevice, ecodes, list_devices, categorize
from xlutils.copy import copy


BARCODEDEVICESTRING = None
DATABASEFILE = None
CASHIER = None
CASHIERMAIL = None


def getdevice():
    """Gets barcode scanner device from name"""
    try:
        devices = map(InputDevice, list_devices())
        for device in devices:
            if device.name == BARCODEDEVICESTRING:
                logging.debug('Device found.')
                dev = InputDevice(device.fn)
                return dev
    except:
        print('Keine Leseberechtigung für Geräte')
        sys.exit(1) 


def readdata(cashiercode, cashieremail):
    """Read data fram barcode scanner"""
    logging.debug('Waiting for data from barcode scanner...')
    displaywait()
    dev = getdevice()

    keys = "X^1234567890XXXXqwertzuiopXXXXasdfghjklXXXXXyxcvbnmXXXXXXXXXXXXXXXXXXXXXXX"

    dev.grab()

    barcode = ''
    lastbarcode = ''

    for event in dev.read_loop():
        if event.type == ecodes.EV_KEY:
            data = categorize(event)
            if data.keystate == 1 and data.scancode != 42: # Catch only keydown, and not Enter
                if data.scancode == 28:
                    logging.debug('Received barcode: %s', barcode)
                    # Kontoauszug senden
                    if barcode == lastbarcode:
                        sendaccountinfo(barcode)
                    elif barcode == cashiercode:
                        senddatabase(cashieremail)
                    elif isnew(barcode):
                        logging.debug('New ID, adding new account: %s', barcode)
                        addaccount(barcode)
                        text = incrementaccount(barcode)
                    else:
                        logging.debug('Incrementing known account: %s', barcode)
                        text = incrementaccount(barcode)
                    displayevent(text['name'], text['amount'])
                    displaywait()
                    lastbarcode = barcode
                    barcode = ""
                else:
                    barcode += keys[data.scancode]


def displayevent(name, amount):
    """Displays an event"""
    os.system('cls' if os.name == 'nt' else 'clear')
    figlet = Figlet(font='slant')
    print(figlet.renderText('\n'))
    print(figlet.renderText('\n'))
    print(figlet.renderText(name))
    print(figlet.renderText('Coffee: ' + str(int(amount))))
    logging.debug('Updating display: ' + name + '\n' + 'Coffee: ' + str(int(amount)))
    time.sleep(5)
    os.system('clear')


def displaywait():
    """Display idle message"""
    figlet = Figlet(font='slant')
    print(figlet.renderText('\n'))
    print(figlet.renderText('Coffeebar v1.0'))
    print(figlet.renderText('\n'))    
    print(figlet.renderText('Scan 2 get your coffee!'))


def displaymailsent(name):
    """Display sent mail"""
    os.system('cls' if os.name == 'nt' else 'clear')
    figlet = Figlet(font='slant')
    print(figlet.renderText('\n'))
    print(figlet.renderText('\n'))
    print(figlet.renderText('Kontoauszug an %s verschickt.' % name))
    time.sleep(5)
    os.system('clear')


def createdatabase():
    """Creates a new Excel file"""
    logging.debug('Creating new Excel file')
    directory = os.path.dirname(DATABASEFILE)
    logging.debug('Checking database diretory existance: %s', directory)
    if not os.path.isdir(directory):
        logging.debug('Creating database directory: %s', directory)
        os.makedirs(os.path.dirname(DATABASEFILE))
    logging.debug('Directory exists.')
    workbook = xlsxwriter.Workbook(DATABASEFILE)
    worksheet = workbook.add_worksheet('Kaffeekasse')

    worksheet.write('A1', 'ID')
    worksheet.write('B1', 'Name')
    worksheet.write('C1', 'Kontostand')
    worksheet.write('D1', 'Email')

    workbook.close()
    logging.debug('Excel file created: %s', DATABASEFILE)


def isnew(barcode):
    """Checks if we´ve got a new or known user"""
    workbook = xlrd.open_workbook(DATABASEFILE)
    worksheet = workbook.sheet_by_name(u'Kaffeekasse')
    for row_index in range(worksheet.nrows):
        thisid = worksheet.cell(row_index, 0).value
        try:
            thisid = str(int(thisid)) # convert to string
        except:
            thisid = str(thisid)

        if barcode in thisid:
            logging.debug('ID matched: %s', barcode)
            return False

    logging.debug('No ID matched: %s', barcode)
    return True


def addaccount(barcode):
    """Adds a new user account"""
    workbook = xlrd.open_workbook(DATABASEFILE)
    worksheet = workbook.sheet_by_index(0)
    row = worksheet.nrows
    wbcopy = copy(workbook)
    sheet = wbcopy.get_sheet(0)
    sheet.write(row, 0, barcode)
    sheet.write(row, 1, 'Neuer Eintrag')
    sheet.write(row, 2, 0)
    sheet.write(row, 3, 'foo@example.com')
    wbcopy.save(DATABASEFILE)
    logging.debug('New account added: %s', barcode)


def incrementaccount(barcode):
    """Increments users amount of consumed coffee"""
    workbook = xlrd.open_workbook(DATABASEFILE)
    worksheet = workbook.sheet_by_name(u'Kaffeekasse')
    wbcopy = copy(workbook)

    row = -1
    for row_index in range(worksheet.nrows):
        thisid = worksheet.cell(row_index, 0).value
        try:
            thisid = str(int(thisid)) # convert to string
        except:
            thisid = str(thisid)

        if barcode in thisid:
            row = row_index
            logging.debug('Increment: ID %s matched in row %d', barcode, row)
            amount = worksheet.cell(row_index, 2).value
            name = worksheet.cell(row_index, 1).value
            logging.debug('Current amount: %d', int(amount))
            amount += 1
    if row != -1:
        sheet = wbcopy.get_sheet(0)
        sheet.write(row, 2, amount)
        wbcopy.save(DATABASEFILE)
        logging.debug('Increased amount (%d) written.', amount)
    return {'name': name, 'amount': amount}


def sendmail(receiver, subject, body, filename = None):
    """Sends account info or Excel sheet as email"""
    logging.debug('Sending mail to %s', receiver)
    parser = ConfigParser()
    parser.read(r'coffeebar.ini')
    mailgunapikey = parser.get('Settings', 'mailgunapikey')
    mailgundomain = parser.get('Settings', 'mailgundomain')
    senderadress = parser.get('Settings', 'mailgunfrom')
    cashiermailaddress = parser.get('Settings', 'cashiermail')
    success = -1

    if filename:
        logging.debug('Attachment: %s', filename)
        logging.debug('Receiver: %s', cashiermailaddress)
        try:
            success = requests.post(
            "https://api.mailgun.net/v3/" + mailgundomain + "/messages",
            auth=("api", "mailgunapikey"),
            files=[("attachment", open(filename))],
            data={"from": "Coffeebar Admin <" + senderadress + ">",
                "to": cashiermailaddress,
                "subject": "Coffeebar Kassenstand",
                "text": "Lieber Kassenwart!\n\nIm Anhang findest du den aktuellen Kassenstand.\n\nDeine Coffeebar",
                "html": "<html>Lieber Kassenwart!\n\nIm Anhang findest du den aktuellen Coffeebar Kassenstand.\n\nDeine Coffeebar</html>"}).status_code
        except:
            logging.critical('Failed sending mail with attachment')
    else:
        logging.debug('Sending account info')
        try:
            success = requests.post(
                "https://api.mailgun.net/v3/" + mailgundomain + "/messages",
                auth=("api", mailgunapikey),
                data={"from": "Coffeebar <" + senderadress + ">",
                    "to": [receiver],
                    "subject": subject,
                    "text": body}).status_code
        except:
            logging.debug('Failed sending mail with account info')

    if success == 200:
        logging.debug('Mail successfully delivered: %d', success)
        return True
    else:
        logging.debug('Failed delivering email: %d', success)
        return False    


def sendaccountinfo(barcode):
    """Send users account info"""
    logging.debug('Sending account information')
    workbook = xlrd.open_workbook(DATABASEFILE)
    worksheet = workbook.sheet_by_name(u'Kaffeekasse')
    for row_index in range(worksheet.nrows):
        thisid = worksheet.cell(row_index, 0).value
        try:
            thisid = str(int(thisid)) # convert to string
        except:
            thisid = str(thisid)

        if barcode in thisid:
            logging.debug('Collecting account details')
            name = worksheet.cell(row_index, 1).value
            receiver = worksheet.cell(row_index, 3).value
            amount = worksheet.cell(row_index, 2).value
            amount = str(int(amount))
            text =  'Hallo ' + name + ',\n\n' + \
                    'dies ist dein Kontoauszug für die Kaffeekasse.\n\n' + \
                    'Dein Kontostand beträgt: ' + amount + ' Tassen Kaffee.\n\n' + \
                    'Beste Grüße von der\n' + \
                    'Coffeebar'

            sendmail(receiver, "Dein Coffeebar Kontostand", text)
            displaymailsent(name)
            displaywait()


def senddatabase(cashieremail):
    """Sends database file to cashier"""
    parser = ConfigParser()
    parser.read(r'coffeebar.ini')
    filename = parser.get('Settings', 'databasefile')
    cashiermailaddress = parser.get('Settings', 'cashiermail')
    sendmail(cashieremail, 'Coffeebar Kassenstand', 
        'Hallo Kassenwart\n\nAnbei der aktuelle Kaffeekassenstand als Excel Datei',
        filename)
    displaymailsent('Kassenbericht')
    displaywait()


if __name__ == "__main__":
    """Here goes the coffee hero"""
    # logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    # logging.basicConfig(filename='../coffeebar.log', level=logging.DEBUG)
    logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='../coffeebar.log',
                    filemode='w')
    logging.debug('Starting Coffeebar')

    os.system('cls' if os.name == 'nt' else 'clear')

    PARSER = ConfigParser()
    logging.debug('Reading configuration')
    PARSER.read('coffeebar.ini')
    DATABASEFILE = PARSER.get('Settings', 'databasefile')
    logging.debug('Database file: %s', DATABASEFILE)
    BARCODEDEVICESTRING = PARSER.get('Settings', 'barcodereader')
    CASHIER = PARSER.get('Settings', 'cashier')
    CASHIERMAIL = PARSER.get('Settings', 'cashiermail')
    logging.debug('Barcode device name: %s', BARCODEDEVICESTRING)

    if not os.path.isfile(DATABASEFILE):
        logging.debug('No database file found.')
        createdatabase()
    
    readdata(CASHIER, CASHIERMAIL)
