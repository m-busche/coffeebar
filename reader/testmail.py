import requests
from configparser import ConfigParser


def sendmail(receiver, subject, body):
    parser = ConfigParser()
    parser.read(r'C:\Users\Markus\PycharmProjects\coffeebar\reader\coffeebar.ini')
    mailgunapikey = parser.get('Settings', 'mailgunapikey')
    mailgundomain = parser.get('Settings', 'mailgundomain')
    senderadress = parser.get('Settings', 'mailgunfrom')
    success = requests.post(
        "https://api.mailgun.net/v3/" + mailgundomain + "/messages",
        auth=("api", mailgunapikey),
        data={"from": "Coffeebar <" + senderadress + ">",
              "to": [receiver],
              "subject": subject,
              "text": body})
    if success.status_code == 200:
        return True
    else:
        return False

if __name__ == "__main__":
    mailsent = sendmail('m.busche@gmail.com', 'Hallo Welt!', 'Mailgun Python Testmail')
    print(str(mailsent))