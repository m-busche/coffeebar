# Automatic login

`sudo nano /etc/systemd/system/getty@tty1.service.d/autologin.conf`

# Settings

Font names and examples: `http://www.figlet.org/examples.html`

# Setup Rasbian

blank Screen der Konsole abschalten

`/boot/cmdline.txt` den Eintrag `consoleblank=0` hinzufügen.